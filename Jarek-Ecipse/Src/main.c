/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t Received;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void START();
void L1(uint32_t,uint32_t,uint32_t);
void L2(uint32_t,uint32_t,uint32_t);
void L3(uint32_t,uint32_t,uint32_t);
void L4(uint32_t,uint32_t,uint32_t);
void L5(uint32_t,uint32_t,uint32_t);
void L6(uint32_t,uint32_t,uint32_t);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void stop();
void idz_prosto();
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_USART1_UART_Init();

  /* USER CODE BEGIN 2 */
  START();
  HAL_UART_Receive_IT(&huart1, &Received, 1);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  //TIM4->CCR2=x; // 71
	  //TIM4->CCR3=x; // 72

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void START(){
	  HAL_TIM_Base_Start_IT(&htim1);
	  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
	  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);
	  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3);
	  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_4);
	  HAL_TIM_Base_Start_IT(&htim2);
	  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
	  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
	  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
	  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);
	  HAL_TIM_Base_Start_IT(&htim3);
	  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);
	  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2);
	  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_3);
	  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_4);
	  HAL_TIM_Base_Start_IT(&htim4);
	  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
	  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_2);
	  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_3);
	  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_4);
	  HAL_TIM_Base_Start_IT(&htim5);
	  HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_1);
	  HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_2);
	  HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_3);
	  HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_4);
}
void L1(uint32_t x,uint32_t y,uint32_t z){
	  TIM1->CCR1=77+x; // 11 45-110
	  TIM2->CCR2=77+y; // 12 45-110
	  TIM3->CCR4=95+z; // 13 55-110
}
void L2(uint32_t x,uint32_t y,uint32_t z){
	  TIM1->CCR2=77+x; // 21
	  TIM1->CCR4=57+y; // 22 //38-103
	  TIM1->CCR3=95+z; // 23
}
void L3(uint32_t x,uint32_t y,uint32_t z){
	  TIM2->CCR3=77+x; // 31
	  TIM2->CCR4=77+y; // 32
	  TIM4->CCR1=95+z; // 33
}
void L4(uint32_t x,uint32_t y,uint32_t z){
	  TIM3->CCR1=75+x; // 41
	  TIM2->CCR1=75+y; // 42
	  TIM4->CCR4=95+z; // 43
}
void L5(uint32_t x,uint32_t y,uint32_t z){
	  TIM3->CCR3=75+x; // 51
	  TIM5->CCR4=75+y; // 52
	  TIM3->CCR2=95+z; // 53
}
void L6(uint32_t x,uint32_t y,uint32_t z){
	  TIM5->CCR1=63+x; // 61
	  TIM5->CCR2=75+y; // 62
	  TIM5->CCR3=95+z; // 63
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	uint8_t Data[50]; // Tablica przechowujaca wysylana wiadomosc.
	uint16_t size = 0; // Rozmiar wysylanej wiadomosci

	// Odebrany znak zostaje przekonwertowany na liczbe calkowita i sprawdzony
	// instrukcja warunkowa
	switch (atoi(&Received)) {
 	 case 0: // Jezeli odebrany zostanie znak 0 stoj
 		 stop();
 		 break;
 	 case 1: // Jezeli odebrany zostanie znak 1 idz do przodu
 		 idz_prosto();
 		 break;
 	 default: // Jezeli odebrano nieobslugiwany znak
 		 break;
	}
	HAL_UART_Receive_IT(&huart1, &Received, 1); // Ponowne włączenie nasłuchiwania
}
void stop(){
	  // pozycje zerowe
	  HAL_Delay(500);
	  L1(0,0,0);
	  L2(0,0,0);
	  L3(0,0,0);
	  L4(0,0,0);
	  L5(0,0,0);
	  L6(0,0,0);
}
void idz_prosto(){
	  uint32_t a=10,b=33,c=33;

	  L1(-a,b,-c);
	  L3(-a,b,-c);
	  L5(a,b,-c);
	  HAL_Delay(500);
	  L1(-a,0,0);
	  L3(-a,0,0);
	  L5(a,0,0);
	  HAL_Delay(1000);
	  L2(a,b,-c);
	  L4(-a,b,-c);
	  L6(-a,b,-c);
	  HAL_Delay(500);
	  L1(a,0,0);
	  L3(a,0,0);
	  L5(-a,0,0);
	  L2(-a,b,-c);
	  L4(a,b,-c);
	  L6(a,b,-c);
	  HAL_Delay(500);
	  L2(-a,0,0);
	  L4(a,0,0);
	  L6(a,0,0);
	  HAL_Delay(1000);
	  L1(a,b,-c);
	  L3(a,b,-c);
	  L5(-a,b,-c);
	  HAL_Delay(500);
	  L2(a,0,0);
	  L4(-a,0,0);
	  L6(-a,0,0);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
